#include <stdio.h>
#include "linked_list.h"

linked_list_t* read_nodes(){
	int value;
	linked_list_t* pointer=NULL;
	puts("Введите целые числа через пробел для заполнения списка:");
	while(scanf("%d",&value)!=EOF){
		if (pointer==NULL) pointer=list_create(value);
		else pointer=list_add_front(value,pointer);
	}
	return pointer;
}

void out_node(linked_list_t* pointer){
	size_t node_index;
	puts("Какой элемент списка вывести?");
	int n=scanf("%zu",&node_index);
	while(1){
		while(n==0){
			scanf("%*s");
			puts("Введите целое число!");
			n=scanf("%zu",&node_index);
		}
		while(n!=EOF && n!=0){
			printf("Значение по индексу %zu: %d\n",node_index,list_get(pointer,node_index));
			puts("Какой элемент списка вывести?");
			n=scanf("%zu",&node_index);
		}
		if (n==EOF) break;
	}
}

void print_nodes(linked_list_t* pointer){
	size_t size=list_length(pointer);
	size_t i;
	puts("");
	for (i=0;i<size;i++){
		printf("Значение %zu:%d\n",i,list_get(pointer,i));
	}
}

int main(int args,char** argv){
	printf("Sizeof linked_list: %zu\n",sizeof(linked_list_t*));
	linked_list_t* pointer=read_nodes();
	print_nodes(pointer);
	printf("Сумма элементов списка:%li\n",list_sum(pointer));
	out_node(pointer);
	list_free(pointer);
}